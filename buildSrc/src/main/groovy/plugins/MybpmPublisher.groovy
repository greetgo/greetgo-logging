package plugins

import org.gradle.configuration.project.ProjectConfigurationActionContainer

import javax.inject.Inject

class MybpmPublisher extends kz.greetgo.gradle.plugins.launchers.GreetgoPublisher {
  @Inject
  MybpmPublisher(ProjectConfigurationActionContainer configurationActionContainer) {
    super(configurationActionContainer)
  }
}
