package kz.greetgo.logging.zookeeper.config;

public interface ZookeeperTry<Result> {
  Result tryOperation() throws Exception;
}
